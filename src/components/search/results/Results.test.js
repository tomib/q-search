import React from 'react';
import { shallow } from 'enzyme';
import Results from './Results';

describe('tests the Results component', () => {
  const results = ['Result 1', 'Result 2', 'Result 3', 'Result 4', 'Result 5'];
  const wrapper = shallow(<Results results={results} term="res" />);

  it('renders the Results component', () => {
    expect(wrapper.find('.search__results').exists()).toBe(true);
  });

  it('renders five Result Items', () => {
    expect(wrapper.find('Item')).toHaveLength(5);
  });
});
