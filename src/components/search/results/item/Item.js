import React from 'react';
import PropTypes from 'prop-types';

import './Item.scss';

const Item = props => (
  <li
    className={`search__result ${props.active ? 'search__result--active' : ''} ${props.selected ? 'search__result--selected' : ''}`}
    onClick={() => props.handleClick(props.title)}
    onKeyUp={() => props.handleClick(props.title)}
    role="presentation"
  >
    {props.title}
  </li>
);

Item.propTypes = {
  active: PropTypes.bool,
  handleClick: PropTypes.func,
  selected: PropTypes.bool,
  title: PropTypes.string.isRequired,
};

Item.defaultProps = {
  active: false,
  handleClick: () => {},
  selected: false,
};

export default Item;
