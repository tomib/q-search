import React from 'react';
import { shallow } from 'enzyme';
import Item from './Item';

describe('tests the Result Item component', () => {
  const wrapper = shallow(<Item active title="Search Result" selected />);

  it('renders the Item', () => {
    expect(wrapper.find('.search__result').exists()).toBe(true);
  });

  it('renders the Item as active', () => {
    expect(wrapper.hasClass('search__result--active')).toEqual(true);
  });

  it('renders the Item as selected', () => {
    expect(wrapper.hasClass('search__result--selected')).toEqual(true);
  });
});
