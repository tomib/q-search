import React from 'react';
import PropTypes from 'prop-types';

import Item from './item/Item';

import './Results.scss';

const Results = props => (
  <ul className="search__results">
    {props.term && props.results.map((item, index) => (
      <Item
        active={props.term.toLowerCase() === item.toLowerCase()}
        key={item}
        handleClick={props.onItemClick}
        selected={props.selected === index}
        title={item}
      />
    ))}
  </ul>
);

Results.propTypes = {
  onItemClick: PropTypes.func,
  results: PropTypes.arrayOf(PropTypes.string),
  selected: PropTypes.number,
  term: PropTypes.string,
};

Results.defaultProps = {
  onItemClick: () => {},
  results: [],
  selected: 0,
  term: '',
};

export default Results;
