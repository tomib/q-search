import React, { Component } from 'react';

import Input from './input/Input';
import Results from './results/Results';
import Button from '../shared/button/Button';

import searchData from '../../assets/first-names.json';

class Search extends Component {
  state = {
    results: [],
    selectedId: 0,
    term: '',
  }

  /* Updating the search term in state whenever the input is changed and recalculating results. */
  onInputChange = (term) => {
    this.setState(() => ({ selectedId: 0, term }), () => this.searchResults());
  }

  /* Detecting ArrowUp, ArrowDown and Enter when Input is targeted. */
  onInputKeyDown = (e) => {
    if (e.key === 'ArrowDown') {
      e.preventDefault();

      /* Selecting the next element in the results list. */
      if (this.state.results.length > 1 && this.state.selectedId < this.state.results.length - 1) {
        this.setState(prevState => ({
          selectedId: prevState.selectedId + 1,
        }));
      }
    } else if (e.key === 'ArrowUp') {
      e.preventDefault();

      /* Selecting the previous element in the results list. */
      if (this.state.selectedId > 0) {
        this.setState(prevState => ({
          selectedId: prevState.selectedId - 1,
        }));
      }
    } else if (e.key === 'Enter') {
      e.preventDefault();

      /* Setting the currently selected element as search term and recalculating results. */
      if (this.state.results.length > 0 && this.state.term) {
        this.setState(() => ({
          selectedID: 0,
          term: this.state.results[this.state.selectedId],
        }), () => this.searchResults());
      }
    }
  }

  /* Setting the search term to the clicked element in the results list. */
  onItemClick = (term) => {
    this.setState(() => ({ selectedId: 0, term }), () => this.searchResults());
  }

  /* Gets the first five results matching the search term from searchData. */
  searchResults = () => {
    const results = [];
    let resultsItems = 0;

    for (let i = 0; i < searchData.length; i += 1) {
      if (searchData[i].toLowerCase().startsWith(this.state.term.toLowerCase())) {
        results.push(searchData[i]);
        resultsItems += 1;
      }

      if (resultsItems > 4) { break; }
    }

    this.setState(() => ({ selectedId: 0, results }));
  }

  render() {
    return (
      <div className="app-page search">
        <h1 className="app-page-title">Q-Search</h1>
        <p>This is the autocomplete search requested in the test task.</p>
        <form>
          <Input
            name="term"
            onChange={e => this.onInputChange(e.target.value)}
            onKeyDown={e => this.onInputKeyDown(e)}
            placeholder="Enter your search term here..."
            value={this.state.term}
          />
          <Results
            onItemClick={this.onItemClick}
            results={this.state.results}
            selected={this.state.selectedId}
            term={this.state.term}
          />
          <Button category="primary" type="submit" title="Search" />
        </form>
      </div>
    );
  }
}

export default Search;
