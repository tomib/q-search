import React from 'react';
import { shallow } from 'enzyme';
import Search from './Search';

describe('tests the Search component', () => {
  const wrapper = shallow(<Search />);

  it('renders the Search component', () => {
    expect(wrapper.find('.search').exists()).toBe(true);
  });

  it('successfully changes the Input value', () => {
    wrapper.find('Input').simulate('change', { target: { value: 'Test Value' } });
    expect(wrapper.state('term')).toBe('Test Value');
  });
});
