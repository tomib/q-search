import React from 'react';
import { shallow } from 'enzyme';
import Input from './Input';

describe('tests the Input component', () => {
  const wrapper = shallow(<Input name="term" />);

  it('renders the Input', () => {
    expect(wrapper.find('.search__input').exists()).toBe(true);
  });
});
