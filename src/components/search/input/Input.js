import React from 'react';
import PropTypes from 'prop-types';

import './Input.scss';

const Input = props => (
  <input
    className="search__input"
    name={props.name}
    onChange={props.onChange}
    onKeyDown={props.onKeyDown}
    placeholder={props.placeholder}
    value={props.value}
  />
);

Input.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  onKeyDown: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.string,
};

Input.defaultProps = {
  onChange: () => {},
  onKeyDown: () => {},
  placeholder: '',
  value: '',
};

export default Input;
