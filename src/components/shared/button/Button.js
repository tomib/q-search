import React from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

const Button = props => (
  <button
    className={`button button--${props.category}`}
    type={props.type}
  >
    {props.title}
  </button>
);

Button.propTypes = {
  category: PropTypes.string,
  title: PropTypes.string,
  type: PropTypes.string,
};

Button.defaultProps = {
  category: 'primary',
  title: 'Button',
  type: 'button',
};

export default Button;
