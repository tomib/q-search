import React from 'react';
import { shallow } from 'enzyme';
import Button from './Button';

describe('tests the Button component', () => {
  const wrapper = shallow(<Button category="primary" title="Search" />);

  it('renders the button', () => {
    expect(wrapper.find('.button').exists()).toBe(true);
  });

  it('renders the button as primary', () => {
    expect(wrapper.hasClass('button--primary')).toEqual(true);
  });

  it('renders the button with the correct text', () => {
    expect(wrapper.text()).toEqual('Search');
  });
});
