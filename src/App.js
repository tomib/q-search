import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Search from './components/search/Search';

import './App.scss';

const App = () => (
  <div className="app">
    <Switch>
      <Route exact path="/search" component={Search} />
      <Redirect to="/search" />
    </Switch>
  </div>
);

export default App;
