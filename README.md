# Q-Search

Demo: http://www.tomislavbisof.com/q-search

Project template based on react-init: https://github.com/tomislavbisof/react-init

### Installation

To run the project, first install all the dependencies using:

```
npm install
```

### Usage

For development with the Webpack Dev Server and HMR, use:

```
npm run watch
```

To build the project for production, use:

```
npm run build
```

Basic unit tests are also available (nothing too indepth due to time limitations):

```
npm run test
```

Your project files will be located in the `/dist/` folder after building. Images smaller than 8KB will be embedded, while larger ones will be located in `/dist/assets/`.